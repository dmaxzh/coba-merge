<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('master');
});

Route::get('/dashboard', function () {
    return view('master_parts.dashboard');
});

Route::get('/table', function () {
    return view('master_parts.table');
});

Route::get('/data-tables', function () {
    return view('master_parts.data_tables');
});

//CRUD Cast implementation

// //menampilkan form untuk membuat data pemain baru
// Route::get('/cast/create', 'CastController@create')->name("cast_create"); 

// //menyimpan data baru ke tabel cast
// Route::post('/cast', 'CastController@store')->name("cast_store");

// //menampilkan list data para pemain film menggunakan html table
// Route::get('/cast', 'CastController@index')->name("cast_index");

// //menangkap id dari url dan menampilkan casters dengan lebih detail
// Route::get('/cast/{id}', 'CastController@show')->name("cast_show");

// //menangkap id dari url dan menampilkan halaman edit
// Route::get('/cast/{id}/edit', 'CastController@edit')->name("cast_edit");

// //menangkap id dari url dan akan mengupdate data dari caster tersebut
// Route::put('/cast/{id}', 'CastController@update')->name("cast_update");

// //menangkap id dari url dan akan menghapus data caster tersebut dari database
// Route::delete('/cast/{id}', 'CastController@destroy')->name("cast_delete");

Route::resource('cast', 'CastController');
