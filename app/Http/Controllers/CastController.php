<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB; //pakai database yang telah diatur di file .env

use App\Cast; //pakai file Cast.php di dalam folder app

class CastController extends Controller
{
    public function create(){
        return view('posts.create_cast');
    }

    public function store(Request $request){
        //dd($request->all());

        //validasi data agar sesuai dengan syarat untuk masuk ke database
        $request->validate([
            'Nama' => 'required|unique:cast',
            'Umur' => 'required',
            'Bio' => 'required'
        ]);

        //bagian query builder Laravel
        //pakai method insert() untuk memasukkan/menyimpan data ke database
        // $query = DB::table('cast')->insert([
        //     "nama" => $request["Nama"], 
        //     "umur" => $request["Umur"], 
        //     'bio' => $request['Bio']
        // ]);

        //bagian ORM Laravel (Eloquent) (dengan method save() untuk memasukkan/menyimpan)
        // $cast = new Cast;
        // $cast->nama = $request["Nama"];
        // $cast->umur = $request["Umur"];
        // $cast->bio = $request["Bio"];
        // $cast->save();

        //bagian ORM Laravel (Eloquent) (dengan method create() untuk memasukkan/menyimpan)
        $cast = Cast::create([
            "nama" => $request["Nama"], 
            "umur" => $request["Umur"], 
            'bio' => $request['Bio']
        ]);



        return redirect('/cast')->with('success', 'Berhasil membuat cast');
    }

    public function index(){
        //$listCasts = DB::table('cast')->get(); //pakai query builder method get()
        $listCasts = Cast::all(); //pakai ORM method all()
        return view('posts.index_cast', compact('listCasts'));
    }

    public function show($id){
        // $catchedCaster = DB::table('cast')->where('id', $id)->first(); //pakai query builder method where()
        $catchedCaster = Cast::find($id); //pakai ORM method find() 
        //method find() selalu menggunakan primarykey
        return view('posts.show_cast', compact('catchedCaster'));
    }

    public function edit($id){
        // $catchedCaster = DB::table('cast')->where('id', $id)->first(); //pakai query builder method where()
        $catchedCaster = Cast::find($id); //pakai ORM method find() 
        //method find() selalu menggunakan primarykey
        return view('posts.edit_cast', compact('catchedCaster'));
    }

    public function update($id, Request $request){
        //dd($request);
        $request->validate([
            'Nama' => 'required',
            'Umur' => 'required',
            'Bio' => 'required'
        ]);

        //menggunakan query builder
        // $editedCaster = DB::table('cast')
        // ->where('id', $id)
        // ->update([
        //     'nama' => $request['Nama'], 
        //     'umur' => $request['Umur'], 
        //     'bio' => $request['Bio']
        // ]);

        //menggunakan ORM
        $editedCaster = Cast::where('id', $id)
        ->update([
            'nama' => $request['Nama'], 
            'umur' => $request['Umur'], 
            'bio' => $request['Bio']
        ]);

        return redirect('/cast')->with('success', 'Berhasil update cast');
    }

    public function destroy($id){
        //$query = DB::table('cast')->where('id', $id)->delete(); //pakai query builder

        // $query = Cast::find($id);
        // $query->delete(); //pakai ORM method delete() //method delete dapat menggunakan parameter lain

        $query = Cast::destroy($id); //pakai ORM method delete() //ethod destroy hanya dapat menggunakan primarykey sebagai parameter
        return redirect('/cast');
    }
}
